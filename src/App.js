import React from 'react'
import './App.css'

// NPM-Packages
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

// React-Bootstrap-Components
import { Navbar, Nav, Container } from 'react-bootstrap'

// Components
import Home from './Components/Home/Home'
import About from './Components/About/About'
import Users from './Components/Users/Users'

function App () {
  return (
    <div className="App">
      <Router>
        <Navbar bg="light" variant="light">
            <Container>
              <Navbar.Brand href="#home">Navbar</Navbar.Brand>
              {/* Navbar-links for diffrent coponents */}
                <Nav className="me-auto">
                  <Nav.Link className="navbar-link-header"><Link to="/home">Home</Link></Nav.Link>
                  <Nav.Link><Link to="/users">Users</Link></Nav.Link>
                  <Nav.Link><Link to="/about">About Us</Link></Nav.Link>
                </Nav>
            </Container>
        </Navbar>
        <Switch>

          {/* Main Component */}
          <Route exact path="/users">
                 <Users />
              </Route>

          {/* Main Component */}
          <Route exact path="/about">
                 <About />
              </Route>

           {/* Main Component */}
           <Route exact path="/home">
                 <Home />
              </Route>

           {/* Main Component */}
              <Route exact path="/">
                 <Home />
              </Route>

        </Switch>
      </Router>

    </div>
  )
}

export default App
