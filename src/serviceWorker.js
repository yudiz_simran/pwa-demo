export default function serviceWorker () {
  const swUrl = `${process.env.PUBLIC_URL}/sw.js`

  navigator.serviceWorker.register(swUrl)
    .then((response) => {
      console.log('registration done', response)
    })

//   if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function () {
//       this.navigator.serviceWorker.register(swUrl)
//         .then(function (registration) {
//           console.log('worker registration is successfully done', registration.scope)
//         }, function () {
//           console.log('failed')
//         })
//         .catch(function (err) {
//           console.log('err', err)
//         })
//     })
//   } else {
//     console.log('Service worker is not supported')
//   }
}
