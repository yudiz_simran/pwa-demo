import React, { useState, useEffect } from 'react'

import { Table, Container, Alert } from 'react-bootstrap'

const Users = () => {
  const [data, setData] = useState([])
  const [mode, setMode] = useState('Online')
  useEffect(() => {
    const url = 'https://jsonplaceholder.typicode.com/users'
    fetch(url).then((response) => {
      response.json().then((result) => {
        console.log('result', result)
        setData(result)
        localStorage.setItem('users', JSON.stringify(result))
      })
    }).catch(() => {
      const collection = localStorage.getItem('users')
      setData(JSON.parse(collection))
      setMode('Offline')
    })
  }, [])
  return (
      <>
        <Container>
        {
        mode === 'Offline'
          ? <div className="mt-2">
             <Alert variant="danger">
            You are in offline mode
           </Alert>
          </div>
          : null
      }
        <h1>Users</h1>
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>USERNAME</th>
                <th>EMAIL</th>
              </tr>
            </thead>
            <tbody>
              {
                data?.map((users) => {
                  return (
                    <>
                      <tr>
                        <td>{users.id}</td>
                        <td>{users.name}</td>
                        <td>{users.username}</td>
                        <td>{users.email}</td>
                      </tr>
                    </>
                  )
                })
              }
            </tbody>
          </Table>
        </Container>
      </>
  )
}

export default Users
