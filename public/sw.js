
const CACHE_NAME = 'codePwa'

// Path activated in offline mode
const urlCache = [
  '/static/js/bundle.js',
  '/static/js/vendors~main.chunk.js',
  '/static/js/main.chunk.js',
  '/manifest.json',
  '/logo192.png',
  '/favicon.ico',
  '/home',
  '/users',
  '/'
]

// install service-worker
this.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then((cache) => {
        return cache.addAll(urlCache)
      })
  )
})

// fetch cache data
this.addEventListener('fetch', (event) => {
  if (!navigator.onLine) {
    event.respondWith(
      caches.match(event.request)
        .then((response) => {
          if (response) {
            return response
          }
          const fUrl = event.request.clone
          fetch(fUrl)
        })
    )
  }
})
